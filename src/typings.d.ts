/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

// to avoid the problem with UglifyJS when doing a Webpack bundle
declare var Tesseract;
