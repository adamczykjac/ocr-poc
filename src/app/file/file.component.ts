import { Component, OnInit, NgZone } from '@angular/core';
import { Samples } from '../static/constants/samples';
import { Locale } from '../static/constants/locale';

@Component({
  selector: 'file-input',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})

export class FileComponent implements OnInit {
  samples: any[];
  selectedSample: any;
  result: string[];
  progress: any;

  constructor(private zone: NgZone) {}

  ngOnInit() {
    this.progress = "Text erkennen";
    this.result = [];

    if(Samples.Files) {
      this.samples = [];
      Samples.Files.forEach((sampleUrl, idx) => {
        let sample = new Object({
          id: idx,
          url: sampleUrl
        });
        if(idx == 0) {
          sample['selected'] = true;
          this.selectedSample = sample;
        }
        else sample['selected'] = false;
        this.samples.push(sample);
      });
    }
  }

  selectHandler(event, selectedSample: any) {
    event.preventDefault();
    this.samples.forEach(sample => {
      sample.id != selectedSample.id ?
        sample.selected = false :
        sample.selected = true
    });
    this.selectedSample = selectedSample;
  }

  recognize(image, event) {
    event.preventDefault();
    Tesseract.recognize(image, { lang: 'deu' })
      .progress(message => {
        this.zone.run(() => {
          this.progress = "";
          if(message.progress)
            this.progress = [
              message.status,
              (message.progress * 100).toFixed(0).toString() + "%"
            ].join(" ")
        });
      })
      .catch(err => this.result = err.message)
      .then(result => {
        this.zone.run(() => {
          this.progress = 'Text erkennen';
          if(result.lines) {
            this.result = result.lines;
          }
        });
      })
      .finally(resultOrError => console.log(resultOrError));
  }
}
