import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'OCR Text-Erkennung PoC';
  message = 'Wählen Sie bitte einen der folgenden Bildern:';
}
