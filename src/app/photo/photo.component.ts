import { Component, OnInit, NgZone } from '@angular/core';
import { Locale } from '../static/constants/locale';

@Component({
  selector: 'photo-input',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})

export class PhotoComponent implements OnInit {
  result: string[];
  progress: any;

  constructor(private zone: NgZone) {}

  ngOnInit() {
    this.progress = "";
    this.result = [];
  }

  onPhoto(event) {
    this.progress = "Warten Sie bitte..."
    this.recognize(event.srcElement.files[0], event);
  }

  recognize(image, event) {
    event.preventDefault();
    Tesseract.recognize(image, { lang: 'deu' })
      .progress(message => {
        this.zone.run(() => {
          this.progress = "";
          if(message.progress)
            this.progress = [
              message.status,
              (message.progress * 100).toFixed(0).toString() + "%"
            ].join(" ")
        });
      })
      .catch(err => this.result = err.message)
      .then(result => {
        this.zone.run(() => {
          this.progress = 'Text erkennen';
          if(result.lines) {
            this.result = result.lines;
          }
        });
      })
      .finally(resultOrError => console.log(resultOrError));
  }
}
