## OCR-Erkennung für Arbeitsunfähigkeitsbescheinigungen

### Was ist bisher möglich:
1. Nür auf der Client-seite:
* Angular2 + TesseractJS[1] [313kB dist] + deutsche Sprache bei Verarbeitung heruntergeladen und zwischengespeichert [969kB]

Vorteile:
* Serverworker nicht erfordert
Nachteile:
* Internetverbindung doch erfordert (die Sprache Herunterladung)

2. Erkennungslogik auf Server-seite - Prototypmaßnahmen:
* TesseractJS [1]
* OpenCV [2]
* Nodejs mit OpenCV bindings [3]
* Queue wie RabbitMQ [4]

Vorteile:
* Sprache nür im Server zwischengespeichert 

Nachteile:
* Internetverbindung erfordert

3. Weitere Fragen:
* Wenn nür die Arbeitsunfähigkeitstermine erfordert, Datenanpassung (MM-TT-JJJJ uzw.) auf CS möglicherweise - JACEK
* Erstbescheinigung vs Folgebescheiningung - ist die Unterscheidung erfordert? - DANIEL

### Quelle
[1] Tesseract Open Source OCR Engine - https://github.com/naptha/tesseract.js#tesseractjs
[2] OpenCV - Open Source Computer Vision Library - http://opencv.org
[3] https://www.npmjs.com/package/opencv
[4] http://www.rabbitmq.com
